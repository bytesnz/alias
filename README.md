# alias
A Postfix email alias manager using Node.js and web sockets

*This is not meant for production purposes - it is mainly for experimenting
with Node.js and web sockets. There is only HTTP Digest authentication.*

`alias` can be used to manage Postfix (or similar) email aliases and reject
lists in a simple web interface. The web interface uses web sockets to
communicate with the server.

The configuration for `alias` is stored as a JSON Object in `config.json`.
The options are:
- `port` *[number]* - port number to listen on
- `address` *[string]* - host name / IP address to listen on
- `aliases` *[Object]*
- `aliases.type` *[string]* - Type of file, currently only regex is supported
- `aliases.path` *[string]* - Path to file
- `reloadCommand` *[string]* - Command to run to reload Postfix once the files
  have been rewritten
- `dbPath` *[string]* - Path to JsonDB to store the aliases in
- `defaultDomain` *[string]* - Default domain for the alias
- `defaultUser` *[string]* - Default destination for alias
- `users` *[Object]* HTTP Basic authentication users with the user as the key
  and the Digest hash for the user. If not set, Digest authentication will
  not be enabled.
- `realm` *[string]* Realm for the HTTP Digest user authentication. Defaults
  to 'alias'

The test user password in *config.json.example* is `test`.

## To Use

1. Clone the repo
```shell
$ git clone https://gitlab.com/bytesnz/alias.git
$ cd alias
```
1. Install it dependencies
```shell
$ yarn
```
1. Create *config.json* file (an example of which is *config.json.example*
1. Ensure the user alias will run as has sudo priviledge to reload postfix
*/etc/sudoers*
```
...
user  ALL = (root) NOPASSWD: /usr/sbin/service postfix reload
```
1. Add the alias reject maps to the Postfix config
*/etc/postfix/main.cf*
```
...
alias_maps = regexp:/etc/postfix/aliases.regex
virtual_alias_maps = regexp:/etc/postfix/aliases.regex
smtpd_recipient_restrictions = regexp:/etc/postfix/reject.regex
```
1. Start alias
```shell
yarn start
```
