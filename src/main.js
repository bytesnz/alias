const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const path = require('path');

const fs = require('fs');
const Promise = require('promise');
const access = Promise.denodeify(fs.access);

const Alias = require('./lib/alias');
const createDigestMiddleware = require('./lib/digest');

const configFile = path.resolve('./config.json');
let config;

access(configFile, fs.R_OK).then(function() {
  config = require(configFile);

  // Setup socket
  return Alias(io, config);
}).then(function(alias) {
  const port = config.port || 6576;
  const address = config.address || "127.0.0.1";

  // Digest authentication
  if (config.users) {
    app.use(createDigestMiddleware(config));
  }

  // Setup static serving of files
  app.use('/', express.static(path.resolve(__dirname, 'web')));

  // Start listening
  http.listen(port, address);

  console.log('alias listening on ' + address + ':' + port);

  alias.reload();
}, function(err) {
  console.error(err.message);
  console.error(err.stack);
});
