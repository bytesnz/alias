const crypto = require('crypto');

const s4 = () => {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(32)
    .substring(1);
};

/**
 * Make a random id from the characters used in base 32 with the first
 * character being an alphabetic character
 *
 * @param {number} length Length of random string to make
 *
 * @returns {string} The random ID
 */
const makeId = (length) => {
  if (!length) {
    length = 9;
  }
  const iterations = Math.ceil((length - 1) / 3);

  let idString = '';

  let i = Math.round(Math.random() * 48);
  if (i > 24) {
    idString += String.fromCharCode(i + 73); // 97 (a) - 24
  } else {
    idString += String.fromCharCode(i + 65); // 65 (A)
  }
  i = 0;
  while (i++ < iterations) {
    idString += s4();
  }

  if (length % 3) {
    return idString.slice(0, length + 1);
  } else {
    return idString;
  }
};

const getClientIp = (req) => req.headers['x-real-ip'] ||
      (
        req.headers['x-forwarded-for'] &&
        req.headers['x-forwarded-for'].replace(/,.*$/, '')
      ) ||
      req.ip ||
      null;

module.exports = function createDigestMiddleware(config) {
  const sessions = {};
  const realm = config.realm || 'alias';

  const forceNewSession = (req, res) => {
    let id;

    do {
      id = makeId(18);
    } while(sessions[id]);

    sessions[id] = {
      time: (new Date()).getTime(),
      nonce: makeId(15),
      nc: [],
      ip: getClientIp(req),
      username: null
    };

    res.set(
      'www-authenticate',
      'Digest ' +
      'realm="' + realm +
      '", nonce="' + sessions[id].nonce +
      '", opaque="' + id +
      '", algorithm="MD5", qop="auth"'
    );

    res.status(401).end();
  };

  const digestMiddleware = (req, res, next) => {
    const authentication = req.get('Authorization');
    if (!authentication) {
      return forceNewSession(req, res);
    }

    const parts = authentication.match(/^Digest (.*="?.*"?(, +|$))+/);

    if (!parts) {
      return forceNewSession(req, res);
    }

    const regEx = /(.*?)="?(.*?)"?(, *|$)/g;
    const details = {
      realm: null,
      username: null,
      uri: null,
      algorithm: null,
      response: null,
      opaque: null,
      qop: null,
      nc: null,
      nonce: null,
      cnonce: null
    };

    while (regEx.lastIndex !== parts[1].length) {
      const part = regEx.exec(parts[1]);
      if (part[1] && part[2]) {
        details[part[1]] = part[2];
      }
    }

    // Find session
    const session = details.opaque && sessions[details.opaque];

    if (!session) {
      return forceNewSession(req, res);
    }

    const ip = getClientIp(req);

    // Check set values
    if (
      session.time < ((new Date()).getTime() - (config.timeout || 600000)) ||
      session.nc.indexOf(parseInt(details.nc, 16)) !== -1 ||
      session.nonce !== details.nonce ||
      session.ip !== ip ||
      details.realm !== realm ||
      (session.username && session.username !== details.username) ||
      !config.users[details.username]
    ) {
      console.warn(
        (new Date()).toISOString() + ' ' + ip + ' failed checks ' +
        'for user ' + details.username
      );
      delete sessions[details.opaque];
      return forceNewSession(req, res);
    }

    session.nc.push(parseInt(details.nc, 16));
    session.username = details.username;

    const ha2 = crypto.createHash('md5').update(
      req.method + ':' + details.uri
    ).digest('hex');
    const response = crypto.createHash('md5').update(
      config.users[details.username] + ':' + session.nonce + ':' +
      details.nc + ':' + details.cnonce + ':auth:' + ha2
    ).digest('hex');

    // Check response
    if (details.response !== response) {
      console.warn(
        (new Date()).toISOString() + ' - ' + ip + ' failed authentication ' +
        'for user ' + details.username
      );
      delete sessions[details.opaque];
      return forceNewSession(req, res);
    }

    next();
  };

  return digestMiddleware;
};
